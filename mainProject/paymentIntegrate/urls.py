from django.urls import path
from . import views


urlpatterns = [
    path('qrcode/', views.StartPaymentByQrView.as_view()),
    path('create_qrcode/', views.CreatePaymentQrView.as_view()),
]