from django.contrib import admin
from .models import QrCode
# Register your models here.

class QrCodeAdmin(admin.ModelAdmin):
    pass

admin.site.register(QrCode, QrCodeAdmin)
