from django.db import models

# Create your models here.
class QrCode(models.Model):
    qrImage = models.FileField(blank=False, null=False, upload_to='images/')