from rest_framework import serializers
from .models import QrCode
from django.conf import settings


class QrSerializer(serializers.ModelSerializer):
    class Meta:
        model = QrCode
        fields = '__all__'
