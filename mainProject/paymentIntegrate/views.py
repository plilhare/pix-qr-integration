from urllib import request

import cv2 as cv
import qrcode
from django.shortcuts import render
from rest_framework.parsers import MultiPartParser, FormParser
from django.views.generic.base import TemplateView
from django.conf import settings
from PIL import Image
from rest_framework import status
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import QrSerializer


class StartPaymentByQrView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, **kwargs):

        # qrcode = request.FILES["qrImage"]
        serializer = QrSerializer(data = request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        img_path = serializer.data["qrImage"]

        im = cv.imread(
            f'/home/pratik/Projects/pixbet-payment-integration/mainProject{img_path}')

        det = cv.QRCodeDetector()
        retval, points, straight_qrcode = det.detectAndDecode(im)

        print(retval)

        qrcode = retval

        data = {
            "qrcode": qrcode,
        }

        # define BASE_URL in settings
        url = f"{settings.BASE_URL}/pix/direct/forintegration/v1/payments/qrcodes"

        try:
            response = request.post(url, json=data)

            if(response):
                return Response(response, {'serializer': serializer}, status=status.HTTP_201_CREATED)
            else:
                return Response(response, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response("something went wrong", status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(serializer.data["qrImage"], status=status.HTTP_200_OK)

    def get(self, request, **kwargs):
        serializer = QrSerializer()

        return Response({'serializer': serializer}, template_name='qrUpload.html')


class CreatePaymentQrView(APIView):

    def post(self, request, **kwargs):

        try:
            category = request.data.get('categoria')
            coin = request.data.get('moeda')
            country = request.data.get('pais')
            city = request.data.get('cidade')
            tx_id = request.data.get('txId')

            img = qrcode.make(
                f"categoria='{category}' moeda='{coin}' pais='{country}' cidade='{city}' txId='{tx_id}'")

            img.save("sample.png")

            return Response("qrcode created", status=status.HTTP_201_CREATED)

        except Exception as e:
            return Response("something went wrong", status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GetPaymentConfirmationView(APIView):

    def get(request, **kwargs):
        try:
            paymentId = request.query_params.get('pagamentoId')

            receiver = request.query_params.get('recebedor')
            value = request.query_params.get('valor')
            free_field = request.query_params.get('campoLivre')

            params ={
                'recebedor':receiver,
                'valor':value,
                'campoLivre':free_field
            }

            url = f"{settings.BASE_URL}/pix/direct/forintegration/v1/payments/{paymentId}/confirmation"

            response = request.get(url, json = params)

            if(response.getcode() == '200'):
                return Response(response,  status=status.HTTP_200_OK)
            elif(response.getcode() == '204'):
                return Response(response,  status=status.HTTP_204_NO_CONTENT)
            else:
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
        
        except Exception as e:
            return Response("something went wrong", status=status.HTTP_500_INTERNAL_SERVER_ERROR)



class ConsultReceiptByE2EID(APIView):

    def get(request, **kwargs):
        e2eid = request.query_params.get('e2eid')

        url = f"{settings.BASE_URL}/pix/direto/forintegration/v1/pix/{e2eid}"

        response = request.get(url)

        if(response.getcode() == '200'):
            return Response(response,  status=status.HTTP_200_OK)
        elif(response.getcode() == '204'):
            return Response(response,  status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(response, status=status.HTTP_400_BAD_REQUEST)