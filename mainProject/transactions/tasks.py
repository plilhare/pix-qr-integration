from celery import shared_task

from pynamodb.transactions import TransactGet, TransactWrite
from pynamodb.exceptions import TransactWriteError, TransactGetError

from .models import BankStatement, TransactionStatement


@shared_task
def transaction(conn, transaction_id, user_id, transaction_type, amount):
    try:
        with TransactGet(connection=conn) as transaction:
            bank_statement = transaction.get(
                BankStatement, user_id)

        result = bank_statement.get()
        balance = result.account_balance

        if(transaction_type == "CREDIT"):
            balance += amount
        elif(transaction_type == "DEBIT"):
            if(balance >= amount):
                balance -= amount
            else:
                return {"status":"insufficient balance", "error":""}

        with TransactWrite(connection=conn) as transaction:
            transaction.update(
                BankStatement(user_id),
                actions=[BankStatement.account_balance.set(balance)],
                condition=(BankStatement.user_id.exists())
            )

        statement = TransactionStatement(
            transaction_id=transaction_id, user_id=user_id, amount=amount, 
            type=transaction_type, account_balance=balance
            )

        
        with TransactWrite(connection=conn) as transaction:
            transaction.save(statement, condition=(
                TransactionStatement.transaction_id.does_not_exist()))

    except ValueError as err:
        return {"status":"ValueError", "error":err}
    except TransactWriteError as err:
        return {"status":"TransactWriteError", "error":err}
    except TransactGetError as err:
        return {"status":"TransactGetError", "error":err}
    
    return {"status":"Successful", "error":""}
