from django.urls import path

from .views import (BankStatementView, CreateTransactionStatementView,
                    TransactionStatementView)


urlpatterns = [
    path('create/', CreateTransactionStatementView.as_view()),
    path('txn-statement/', TransactionStatementView.as_view()),
    path('bank-statement/', BankStatementView.as_view())
]