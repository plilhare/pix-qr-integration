from datetime import datetime

import pytz
from decouple import config
from pynamodb.attributes import (BooleanAttribute, NumberAttribute,
                                 UnicodeAttribute, UTCDateTimeAttribute)
from pynamodb.indexes import AllProjection, GlobalSecondaryIndex
from pynamodb.models import Model


host = config('host')
utc_tz = pytz.timezone('UTC')


class BankStatement(Model):
    class Meta:
        table_name = 'BankStatement'
        host = host
        write_capacity_units = 1
        read_capacity_units = 1

    user_id = UnicodeAttribute(hash_key=True)
    account_balance = NumberAttribute(default=0)
    is_active = BooleanAttribute()


class TransactionStatement(Model):
    class Meta:
        table_name = 'TransactionStatement'
        host = host
        write_capacity_units = 1
        read_capacity_units = 1

    transaction_id = UnicodeAttribute(hash_key=True)
    date = UTCDateTimeAttribute(range_key=True, default=datetime.now(utc_tz))
    user_id = UnicodeAttribute()
    amount = NumberAttribute()
    account_balance = NumberAttribute()
    type = UnicodeAttribute(null=False)
    is_active = BooleanAttribute(default=True)


if not BankStatement.exists():
    BankStatement.create_table()

if not TransactionStatement.exists():
    TransactionStatement.create_table()