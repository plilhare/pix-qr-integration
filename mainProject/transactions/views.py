from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import BankStatement, TransactionStatement
import json
from pynamodb.connection import Connection
from pynamodb.transactions import TransactGet, TransactWrite
from pynamodb.exceptions import TransactWriteError, TransactGetError
from decouple import config
from .tasks import transaction


HOST = config('host')
REGION = config('region')


class BankStatementView(APIView):

    def post(self, request):
        user_id = request.data.get('user_id')

        try:
            conn = Connection(host=HOST, region=REGION)

            with TransactGet(connection=conn) as transaction:
                user_statement = transaction.get(
                    BankStatement, user_id)

            result = user_statement.get()

            response = {"user_id": result.user_id,
                        "balance": result.account_balance}

        except Exception as err:
            return Response(f"{err}", status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(response, status=status.HTTP_200_OK)


class TransactionStatementView(APIView):

    def post(self, request):
        transaction_id = request.data.get('transaction_id')

        try:
            conn = Connection(host=HOST, region=REGION)

            with TransactGet(connection=conn) as transaction:
                user_statement = transaction.get(
                    TransactionStatement, transaction_id)

            result = user_statement.get()

            response = {"transaction_id": result.transaction_id, "user_id": result.user_id,
                        "amount": result.amount, "balance": result.account_balance, 
                        "transaction_type": result.type, "date": result.date}

        except Exception as err:
            return Response(f"{err}", status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(response, status=status.HTTP_200_OK)


class CreateTransactionStatementView(APIView):

    def post(self, request):
        transaction_id = request.data.get('transaction_id')
        user_id = request.data.get('user_id')
        transaction_type = request.data.get('transaction_type')
        amount = request.data.get('amount')

        
        conn = Connection(host=HOST, region=REGION)
        response = transaction(conn, transaction_id, user_id, transaction_type, amount)

        if response["status"] == "insufficient balance":
            return Response({"error": "Insufficient balance"}, status=status.HTTP_204_NO_CONTENT)
        elif response["status"] in ["ValueError","TransactGetError","TransactWriteError"]:
            Response({"error":f"{response.error}"}, status = status.HTTP_500_INTERNAL_SERVER_ERROR)

        if response["status"] == "Successful":
            return Response({"Successful"}, status=status.HTTP_201_CREATED)
